/*
 * This software may be modified and distributed under the terms of
 * a BSD-style license.  See the COPYING file in the package base
 * directory for details.
 *
 */
#include <otf2/otf2.h>
#include <otf2/OTF2_GeneralDefinitions.h>
#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <chrono>
#include <iomanip>

#include<stdio.h>


void show_usage(int argc, char** argv)
{
	std::cout << "Usage: " << argv[0] << " [option] trace_file.otf2" << std::endl;
	std::cout << std::endl;
	std::cout << "\t-h\tShow help" << std::endl;
}

static OTF2_CallbackCode Enter_print(OTF2_LocationRef location,
                                     OTF2_TimeStamp time,
                                     void* userData,
                                     OTF2_AttributeList* attributes,
                                     OTF2_RegionRef region)
{
	std::cout << "Entering region " << region << " at location " << location << " at time " << time << std::endl;
	return OTF2_CALLBACK_SUCCESS;
}

static OTF2_CallbackCode Leave_print(OTF2_LocationRef location,
                                     OTF2_TimeStamp time,
                                     void* userData,
                                     OTF2_AttributeList* attributes,
                                     OTF2_RegionRef region)
{
	std::cout << "Leaving region " << region << " at location " << location << " at time " << time << std::endl;

	return OTF2_CALLBACK_SUCCESS;
}

static uint64_t number_of_locations;

struct otf2_location
{
	OTF2_LocationRef location;
	OTF2_LocationGroupRef mpi_rank;

	std::vector<size_t> data_sent;
};

static std::map<OTF2_LocationRef, struct otf2_location*> locations;
// need to map a MPI_rank with its location
static std::map<uint32_t, OTF2_LocationRef> rank_to_location;

void print_matrix()
{
	for (auto it = locations.begin(); it != locations.end(); ++it)
	{
		struct otf2_location* l = (*it).second;
		for (int j = 0; j < number_of_locations; j++)
		{
			size_t data_sent = l->data_sent[j];

			printf("%c%lu", j == 0 ? ' ' : ',', data_sent);
		}
		printf("\n");
	}
}

static OTF2_CallbackCode GlobDefLocation_Register(void* userData,
                                                  OTF2_LocationRef location,
                                                  OTF2_StringRef name,
                                                  OTF2_LocationType locationType,
                                                  uint64_t numberOfEvents,
                                                  OTF2_LocationGroupRef locationGroup)
{
	struct otf2_location* l = new otf2_location();
	l->location = location;
	l->mpi_rank = locationGroup;
	l->data_sent.reserve(number_of_locations);
	for (int i = 0; i < number_of_locations; i++)
		l->data_sent[i] = 0;

	locations[location] = l;
	// initialize mapping MPI_rank to locationRef for callback MpiSend and MpiIsend
	rank_to_location[locationGroup] = location;

	return OTF2_CALLBACK_SUCCESS;
}


OTF2_CallbackCode callback_MpiSend(OTF2_LocationRef sender,
                                   OTF2_TimeStamp time,
                                   uint64_t eventPosition,
                                   void* userData,
                                   OTF2_AttributeList*/*attributes*/,
                                   uint32_t receiver,
                                   OTF2_CommRef communicator,
                                   uint32_t msgTag,
                                   uint64_t length)
{
	struct otf2_location* s = locations[sender];
	struct otf2_location* r = locations[rank_to_location[receiver]];
	s->data_sent[r->mpi_rank] += length;

	return OTF2_CALLBACK_SUCCESS;
}

OTF2_CallbackCode callback_MpiIsend(OTF2_LocationRef sender,
                                    OTF2_TimeStamp time,
                                    uint64_t eventPosition,
                                    void* userData,
                                    OTF2_AttributeList* attributeList,
                                    uint32_t receiver,
                                    OTF2_CommRef communicator,
                                    uint32_t msgTag,
                                    uint64_t length,
                                    uint64_t /*requestID*/)
{
	struct otf2_location* s = locations[sender];
	struct otf2_location* r = locations[rank_to_location[receiver]];
	s->data_sent[r->mpi_rank] += length;

	return OTF2_CALLBACK_SUCCESS;
}


void open_locations(OTF2_Reader* reader)
{
	OTF2_Reader_GetNumberOfLocations(reader,
	                                 &number_of_locations);

	OTF2_GlobalDefReader* global_def_reader = OTF2_Reader_GetGlobalDefReader(reader);

	OTF2_GlobalDefReaderCallbacks* global_def_callbacks = OTF2_GlobalDefReaderCallbacks_New();
	OTF2_GlobalDefReaderCallbacks_SetLocationCallback(global_def_callbacks,
	                                                  &GlobDefLocation_Register);

	OTF2_Reader_RegisterGlobalDefCallbacks(reader,
	                                       global_def_reader,
	                                       global_def_callbacks,
	                                       NULL);

	uint64_t definitions_read = 0;
	OTF2_Reader_ReadAllGlobalDefinitions(reader,
	                                     global_def_reader,
	                                     &definitions_read);
	OTF2_EvtReaderCallbacks* event_callbacks = OTF2_EvtReaderCallbacks_New();
	OTF2_EvtReaderCallbacks_SetMpiSendCallback(event_callbacks, &callback_MpiSend);
	OTF2_EvtReaderCallbacks_SetMpiIsendCallback(event_callbacks, &callback_MpiIsend);

	for (auto it = locations.begin(); it != locations.end(); ++it)
	{
		OTF2_EvtReader* evt_reader = OTF2_Reader_GetEvtReader(reader, (*it).second->location);
		OTF2_Reader_SelectLocation(reader, (*it).second ->location);
		OTF2_Reader_RegisterEvtCallbacks(reader, evt_reader, event_callbacks, nullptr);

#if 0
  		OTF2_GlobalEvtReaderCallbacks_SetMpiIsendCompleteCallback(_global_evt_callbacks, &callback_MpiIsendComplete);
  		OTF2_GlobalEvtReaderCallbacks_SetMpiIrecvRequestCallback(_global_evt_callbacks, &callback_MpiIrecvRequest);
  		OTF2_GlobalEvtReaderCallbacks_SetMpiRecvCallback(_global_evt_callbacks, &callback_MpiRecv);
  		OTF2_GlobalEvtReaderCallbacks_SetMpiIrecvCallback(_global_evt_callbacks, &callback_MpiIrecv);
#endif
#if 0
  		OTF2_GlobalEvtReaderCallbacks_SetEnterCallback( event_callbacks,
								  &Enter_print );
  		OTF2_GlobalEvtReaderCallbacks_SetLeaveCallback( event_callbacks,
								  &Leave_print );
#endif
		uint64_t events_read = 0;
		OTF2_Reader_ReadAllLocalEvents(reader, evt_reader, &events_read);
		OTF2_Reader_CloseEvtReader(reader, evt_reader);
	}
}


int main(int argc, char** argv)
{
	int nb_opts = 0;
	for (int i = 1; i < argc; i++)
	{
		if (!std::strcmp(argv[i], "-h"))
		{
			show_usage(argc, argv);
			return EXIT_SUCCESS;
		}
	}

	if (argc <= nb_opts + 1)
	{
		show_usage(argc, argv);
		return EXIT_FAILURE;
	}

	// Setting the OTF2 File
	std::string otf2_file = argv[nb_opts + 1];

	/* Starting processing of trace */
	std::cout << "[otf2-comm-matrix] Starting matrix computation..." << std::endl;
	auto start = std::chrono::high_resolution_clock::now();

	OTF2_Reader* reader = OTF2_Reader_Open(otf2_file.c_str());
	if (reader == NULL)
	{
		std::cout << "Failed to open " << otf2_file.c_str() << std::endl;
		return EXIT_FAILURE;
	}


	OTF2_GlobalDefReader* global_def_reader = OTF2_Reader_GetGlobalDefReader(reader);
	if (global_def_reader == NULL)
	{
		std::cout << "Failed to open " << otf2_file.c_str() << std::endl;
		return EXIT_FAILURE;
	}

	OTF2_GlobalDefReaderCallbacks* global_def_callbacks = OTF2_GlobalDefReaderCallbacks_New();
	if (global_def_callbacks == NULL)
	{
		std::cout << "Failed to open " << otf2_file.c_str() << std::endl;
		return EXIT_FAILURE;
	}

	uint64_t number_of_locations;
	OTF2_Reader_GetNumberOfLocations(reader,
	                                 &number_of_locations);
	open_locations(reader);


	OTF2_GlobalDefReaderCallbacks_Delete(global_def_callbacks);

	OTF2_Reader_Close(reader);

	print_matrix();


	/* Print program duration */
	auto end = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
	std::cout << std::endl << "[otf2-comm-matrix] *** Elapsed time: " << std::setw(7) << duration.count() <<
		" nanoseconds. ***" << std::endl;

	return EXIT_SUCCESS;
}
